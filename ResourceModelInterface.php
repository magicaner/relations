<?php
interface Relation_ResourceModelInterface
{
    /**
     * relations configuration
     *
     * @return array
     */
    public function relations();

    /**
     * get relation instance
     *
     * @param string $name
     * @return null|Relation2_Base
     */
    public function getRelation($name, $object);

    /**
     * init relation
     *
     * @param string $name
     * @param array  $config
     * @return Relation|null
     */
    public function initRelation($name, $config);

    /**
     * init relation by name
     *
     * @param string $name
     * @return Relation|null
     */
    public function initRelationByName($name);

    /**
     * get relation name by model name
     *
     * @param $needleModelName
     * @return null|string
     */
    public function getRelationNameByModelClassName($needleModelClassName);


    /**
     * load relations
     *
     * @param Varien_Object $object
     * @return $this
     */
    public function loadRelations($object, $names = null);
}
