<?php
/**
 * @method Relation_ResourceModel getResource()
 * @package Relation
 * @author Misha Medgitov <medgitov@gmail.com>
 */

trait Relation_ResourceCollectionTrait
{
    /**
     * joined relations to current select
     *
     * @var array
     */
    protected $_joinedRelations = [];

    protected $_relationParentModel = null;

    /**
     * set parent model
     *
     * @param string $relationName
     * @param Relation_ModelTrait $model
     * @return $this
     */
    public function setRelationParentModel($relationName, $model)
    {
        $this->_relationParentModel = [$relationName => $model];
        return $this;
    }

    /**
     * is relation joined
     *
     * @param string $relationName
     * @return bool
     */
    public function isJoined($relationName)
    {
        return isset($this->_joinedRelations[$relationName]);
    }

    /**
     * join relation to query
     *
     * @param string $relationName
     * @param string $cols
     * @param string $joinType
     * @return $this
     */
    public function joinRelation($relationName, $cols = [], $joinType = Zend_Db_Select::INNER_JOIN)
    {
        if (isset($this->_joinedRelations[$relationName])) {
            return $this;
        }

        $resourceModel = $this->getResource();
        /** @var $relation Relation_Base */
        if (!($relation = $resourceModel->initRelationByName($relationName))) {
            throw new Relation_Exception('Relation configuration not found');
        }

        switch ($relation->getType()) {
            case Relation_Base::MM:
                $this->joinManyToManyRelation($relation, $cols, $joinType);
                break;
            case Relation_Base::HM:
            case Relation_Base::HO:
            case Relation_Base::BT:
                $this->joinHasAnyRelation($relation, $cols, $joinType);
                break;
        }

        return $this;
    }

    /**
     * join many to many relation to query
     *
     * @param Relation_Base $relation
     * @param string $joinType
     * @return $this
     */
    public function joinManyToManyRelation($relation, $cols = [], $joinType = Zend_Db_Select::INNER_JOIN)
    {
        $resourceModel = $this->getResource();
        $relationResourceModel = Mage::getModel($relation->getModel())->getResource();
        $flippedMediatorConfig = array_flip($relation->getMediatorConfig());

        $mediatorFk = $flippedMediatorConfig[$relation->getFkAlias()];
        $mediatorPk = $flippedMediatorConfig[$relation->getPkAlias()];

        $mediatorTable = $resourceModel->getTable($relation->getMediatorTable());
        if ($relationResourceModel instanceof Mage_Eav_Model_Entity_Abstract) {
            $relationTable = $relationResourceModel->getEntityTable();
        } else {
            $relationTable = $relationResourceModel->getMainTable();
        }

        $from = $this->getSelect()->getPart('from');
        reset($from);
        list($aliasName, $tableName) = each($from);

        $this
            ->joinByType(
                $joinType,
                [$mediatorTable => $mediatorTable],
                $aliasName . '.' . $relation->getFk() .
                ' = ' .
                $mediatorTable . '.' . $mediatorFk,
                []
            )
            ->joinByType(
                $joinType,
                [$relation->getName() => $relationTable],
                $mediatorTable . '.' . $mediatorPk .
                ' = ' .
                $relation->getName() . '.' . $relation->getPk(),
                $cols
                //[$relation->getName() . '_' . $relation->getPk() => $relationTable . '.' . $relation->getPk()]
            );

        $this->_joinedRelations[$relation->getName()] = true;
        return $this;
    }
    /**
     * join many to many relation to query
     *
     * @param Relation_Base $relation
     * @param string $joinType
     * @return $this
     */
    public function joinHasAnyRelation($relation, $cols = [], $joinType = Zend_Db_Select::INNER_JOIN)
    {
        $resourceModel = $this->getResource();
        $relationResourceModel = Mage::getModel($relation->getModel())->getResource();
        if (method_exists($relationResourceModel, 'getEntityTable')) {
            $relationTable = $relationResourceModel->getEntityTable();
        } else {
        $relationTable = $relationResourceModel->getMainTable();
        }
        $this
            ->joinByType(
                $joinType,
                [$relation->getName() => $relationTable],
                'main_table.' . $relation->getFk() .
                ' = ' .
                $relation->getName() . '.' . $relation->getPk(),
                $cols
                //[$relation->getName() . '_' . $relation->getPk() => $relationTable . '.' . $relation->getPk()]
            );

        $this->_joinedRelations[$relation->getName()] = true;
        return $this;
    }

    /**
     * join by type
     *
     * @param string $type
     * @param string $table
     * @param string $cond
     * @param string $cols
     * @return $this
     */
    public function joinByType($type, $table, $cond, $cols = '*')
    {
        $parts = explode(' ', $type);
        if (isset($parts[1])) {
            $method = $parts[1] . ucfirst($parts[0]);
        } else {
            $method = $parts[0];
        }

        if (is_array($table)) {
            foreach ($table as $k => $v) {
                $alias = $k;
                $table = $v;
                break;
            }
        } else {
            $alias = $table;
        }

        if (strpos($table, '/') !== false) {
            $table = $this->getTable($table);
        }

        if (!isset($this->_joinedTables[$alias])) {
            $this->getSelect()->{$method}(
                array($alias => $table),
                $cond,
                $cols
            );
            $this->_joinedTables[$alias] = true;
        }
        return $this;
    }

    /**
     * Redeclare after load method for specifying collection items original data
     *
     * @return $this
     */
    protected function _afterLoad()
    {
        if ($this->_relationParentModel) {
            list($relationName, $model) = each($this->_relationParentModel);
        }
            foreach ($this->_items as $item) {
            if (isset($relationName) && isset($model)) {
                $item->setData($relationName, $model);
            }
            $item->setDataChanges(false);
        }
        return parent::_afterLoad();
    }

    /**
     * to string, merge all ids
     *
     * @return string
     */
    public function __toString()
    {
        return implode(',' ,$this->getAllIds());
    }

    /**
     * set collection as empty
     */
    public function setEmpty()
    {
        $this->_data = [];
        $this->_isCollectionLoaded = true;
    }
}
