<?php
/**
 * @author Misha Medgitov <medgitov@gmail.com>
 */

/**
 * configuration example:
 *
 *  [
 *      'store' => [
 *          'hasOne' => 'catalog/product',
 *          // current table primary key => coresponding table foreign key
 *          'entity_id' => 'product_id',
 *      ],
 *  ]
 *
 * Class HasManyRelation
 */
class Relation_HasMany extends Relation_Base
{
    /**
     * save relation
     *
     * @param Traversable $values
     * @return $this
     */
    public function save($values)
    {
        if ($this->_readonly == true) {
            return $this;
        }

        $existingItems = $this->getInstance();
        $newItems = [];

        if (is_array($values) || $values instanceof Traversable) {
            foreach ($values as $item) {
                if (!is_a($item, $this->getModelClassName())){
                    if (is_array($item)) {
                        $data = $item;
                        $item = Mage::getModel($this->getModel());
                        $item->addData($data);
                    } elseif ($item instanceof Varien_Object) {
                        $data = $item->getData();
                        $item = Mage::getModel($this->getModel());
                        $item->addData($data);
                    }
                }
                if (is_a($item, $this->getModelClassName())) {
                    $newItems[] = $item;
                }
            }
        }

        foreach ($existingItems as $item) {
            if (false === ($index = $this->_isItemInArray($item, $newItems))) {
                $item->delete();
            } else {
                unset($newItems[$index]);
            }
        }
        foreach ($existingItems as $item) {
            $item->getResource()->save($item);
        }
        foreach ($newItems as $item) {
            $item->setId(null);
            $item->setData($this->getPk(), $this->getModelInstance()->getData($this->getFk()));
            $item->getResource()->save($item);
        }
        return $this;
    }

    /**
     * check if item exists in given array of objects
     *
     * @param Varien_Object $needle
     * @param array         $haystack
     * @return bool
     */
    protected function _isItemInArray($needle, $haystack)
    {
        foreach ($haystack as $index => $item) {
            if ($this->compareObjects($needle, $item)) {
                return $index;
            }
        }
        return false;
    }

    /**
     * compare objects
     *
     * @param Varien_Object $objLeft
     * @param Varien_Object $objRight
     * @return bool
     */
    protected function compareObjects($objLeft, $objRight)
    {
        foreach ($objRight->getData() as $key => $value) {
            if (strcasecmp($objLeft->getData($key), $value) != 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * get relation instance
     *
     * @return mixed
     */
    public function getInstance($data=[])
    {

        $model = $this->getModelInstance();
        /** @var Varien_Data_Collection_Db $collection */

        $collection = Mage::getResourceModel($this->getModel() . '_collection');

        $fk = $model->getData($this->getFk());
        if ($collection instanceof Relation2_ResourceCollectionInterface) {
            if ($fk) {
                $collection->addFieldToFilter($this->getPk(), ['in' => $fk]);
            } else {
                $collection->setEmpty();
            }
        } else {
            $collection->addFieldToFilter($this->getPk(), ['in' => $fk]);
        }

        if (
            ($collection instanceof Relation_ResourceCollectionInterface) &&
            ($relationInstance = $this->_getReverseRelation($model))
        ) {
            $collection->setRelationParentModel($relationInstance->getName(), $model);
        }

        return $collection;
    }

}
