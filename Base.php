<?php
/**
 * @package Relation
 * @author Misha Medgitov <medgitov@gmail.com>
 */

/**
 * Class Relation
 *
 * @method string getName()
 * @method string getType()
 * @method string getModel() usable at Mage::getModel() as parameter
 * @method string getFk()
 * @method string getPk()
 * @method string getFkAlias()
 * @method string getPkAlias()
 * @method string getMediatorTable()
 * @method array getMediatorConfig()
 * @method string getModelClassName()
 */
abstract class Relation_Base extends Varien_Object
{
    const MM = 'ManyToMany';
    const HM = 'HasMany';
    const HO = 'HasOne';
    const BT = 'BelongsTo';

    protected $_tables = [];
    protected $_readonly = false;

    /**
     * @var Mage_Core_Model_Abstract
     */
    protected $_modelInstance = null;

    /**
     * constructor
     *
     * @param string $name
     * @param array  $config
     *
     * @return void
     */
    public function __construct($name, &$config)
    {
        reset($config);
        list($relationType, $relationModel) = each($config);
        list($relationFK, $relationPK) = each($config);
        list($intermediaryTable, $intermediaryTableConfig) = each($config);

        @list($pk, $pkAlias) = @explode(':', $relationPK);
        @list($fk, $fkAlias) = @explode(':', $relationFK);

        @list($name, $staticFieldName) = @explode(':', $name);

        $this->addData([
            'type' => $relationType,
            'name' => $name,
            'model' => $relationModel,
            'model_class_name' => Mage::getConfig()->getModelClassName($relationModel),
            'fk' => $fk,
            'fk_alias' => $fkAlias ?: $fk,
            'pk' => $pk,
            'pk_alias' => $pkAlias ?: $pk,
            'mediator_table' => $intermediaryTable,
            'mediator_config' => $intermediaryTableConfig
        ]);

        if (isset($config['readonly'])) {
            $this->_readonly = (bool)$config['readonly'];
        }
    }

    /**
     * save values
     *
     * @param Traversable $values
     * @return $this
     */
    abstract public function save($values);

    /**
     * get relation instance
     *
     * @return mixed
     */
    abstract public function getInstance($data=[]);


    /**
     * set model instance
     *
     * @param Mage_Core_Model_Abstract $model
     * @return $this
     */
    public function setModelInstance($model)
    {
        $this->_modelInstance = $model;
        return $this;
    }

    /**
     * get model instance
     *
     * @return Mage_Core_Model_Abstract
     */
    public function getModelInstance()
    {
        return $this->_modelInstance;
    }

    /**
     * get connection
     *
     * @return Varien_Db_Adapter_Interface
     */
    public function getReadConnection()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_read');
    }

    /**
     * get connection
     *
     * @return Varien_Db_Adapter_Interface
     */
    public function getWriteConnection()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_write');
    }


    /**
     * Get table name (validated by db adapter) by table placeholder
     *
     * @param string|array $tableName
     * @return string
     */
    public function getTable($tableName)
    {
        $cacheKey = $this->_getTableCacheName($tableName);
        if (!isset($this->_tables[$cacheKey])) {
            $this->_tables[$cacheKey] = Mage::getSingleton('core/resource')->getTableName($tableName);
        }
        return $this->_tables[$cacheKey];
    }

    /**
     * Retrieve table name for cache
     *
     * @param string|array $tableName
     * @return string
     */
    protected function _getTableCacheName($tableName)
    {
        if (is_array($tableName)) {
            return join('_', $tableName);

        }
        return $tableName;
    }

    /**
     * get reverse relation
     *
     * @param mixed $model
     * @param null $relationModel
     * @return null|Relation
     */
    protected function _getReverseRelation($model, $relationModel = null)
    {
        /** @var Relation_ResourceModelTrait $relationResource */
        if (!$relationModel) {
            $relationResource =  Mage::getResourceModel($this->getModel());
        } else {
            $relationResource = $relationModel->getResource();
        }

        $found = false;
        if ($relationResource instanceof Relation_ResourceModelInterface) {
            foreach ($relationResource->relations() as $name => $relation) {
                $relationInstance = $relationResource->initRelationByName($name);

                if (!$relationInstance) {
                    continue;
                }

                $comparisonData = [
                    'type' => self::BT,
                    'pk' => $this->getFk(),
                    'fk' => $this->getPk(),
                    'model' => $model->getResourceName()
                ];
                $found = true;
                foreach ($comparisonData as $k => $v) {
                    if ($relationInstance->getData($k) != $v) {
                        $found = false;
                        break;
                    }
                }

                if ($found) {
                    break;
                }
            }
        }

        if ($found) {
            return $relationInstance;
        }

        return null;
    }

    /**
     * assign revers link
     *
     * @param mixed $model
     * @param mixed $relationModel
     */
    protected function _assignReverseLink($model, $relationModel)
    {
        if (
            ($relation = $this->_getReverseRelation($model, $relationModel))
            && !$relationModel->hasData($relation->getName())
        ) {
            $relationModel->setData($relation->getName(), $model);
        }
    }
}
