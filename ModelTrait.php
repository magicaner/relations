<?php
/**
 * @author Misha Medgitov <medgitov@gmail.com>
 */

trait Relation_ModelTrait
{
    /**
     * @param array|null $names
     * @return $this
     */
    public function loadRelations($names = null)
    {
        $this->getResource()->loadRelations($this, $names);
        return $this;
    }
    /**
     * get relation instance
     *
     * @param string $name
     * @return null|Relation_Base
     */
    public function getRelation($name)
    {
        return $this->getResource()->getRelation($name, $this);
    }


}
