<?php
/**
 * @author Misha Medgitov <medgitov@gmail.com>
 */

/**
 * configuration example:
 *
 *  [
 *      'store' => [
 *          'hasOne' => 'catalog/product',
 *          // current table primary key => coresponding table foreign key
 *          'entity_id' => 'product_id',
 *      ],
 *  ]
 *
 * Class HasOneRelation
 */
class Relation_HasOne extends Relation_HasMany
{
    /**
     * get relation instance
     *
     * @return mixed
     */
    public function getInstance($data=[])
    {
        $model = $this->getModelInstance();
        /** @var Varien_Data_Collection_Db $collection */
        $collection = Mage::getResourceModel($this->getModel() . '_collection');

        $fk = $model->getData($this->getFk());
        if ($collection instanceof Relation_ResourceCollectionInterface) {
            if ($fk) {
                $collection->addFieldToFilter($this->getPk(), ['in' => $fk]);
            } else {
                $collection->setEmpty();
            }
        } else {
            $collection->addFieldToFilter($this->getPk(), ['in' => $fk]);
        }

        $collection->walk('afterLoad');

        $relationModel = $collection->getFirstItem();
        if (!empty($data)) {
            $relationModel->addData($data);
        }
        if ($relationModel->getId()) {
            $this->_assignReverseLink($model, $relationModel);
            return $relationModel;
        }

        return null;
    }

}
