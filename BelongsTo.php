<?php
/**
 * @author Misha Medgitov <medgitov@gmail.com>
 */

/**
 * configuration example:
 *
 *  [
 *      'store' => [
 *          'belongsTo' => 'core/store',
 *          'store_id' => 'store_id',
 *      ],
 *  ]
 *
 * Class BelongsToRelation
 */
class Relation_BelongsTo extends Relation_Base
{
    /**
     * save relation
     *
     * @param Traversable $values
     * @return $this
     */
    public function save($values)
    {
        return $this;
    }

    /**
     * get relation instance
     *
     * @return mixed
     */
    public function getInstance($data=[])
    {
        $model = $this->getModelInstance();
        /** @var Varien_Data_Collection_Db $collection */
        $fkValue = $model->getData($this->getFk());
        $relatedModel = Mage::getModel($this->getModel());
        if (!empty($data)) {
            $relatedModel->addData($data);
        }
        if ($relatedModel->getIdFieldName() == $this->getPk()) {
            $relatedModel->load($fkValue);
        } else {
            $relatedModel->load($fkValue, $this->getPk());
        }

        if ($relatedModel->getId()) {
            $this->_assignReverseLink($model, $relatedModel);
            return $relatedModel;
        }

        return null;
    }
}
