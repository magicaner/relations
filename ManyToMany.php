<?php
/**
 * @author Misha Medgitov <medgitov@gmail.com>
 */

/**
 * configuration example:
 *
 *  public function relations()
 *  {
 *      return [
 *          'customer_groups' => [
 *              // relation type => // related model
 *              Relation_Base::MM => 'customer/customer_group',
 *
 *              // current table primary key => related table primary key
 *              'entity_id:alias' => 'customer_group_id:alias',
 *
 *              // intermediary table config
 *              'ecommeleon_bookkeeping/ecommeleon_bookkeeping_report_customer_group' => [
 *
 *                  // left operand is field of intermediary table
 *                  // right operand is field of related tables
 *
 *                  'report_id' => 'entity_id', // links to current table primary key
 *                  'customer_group_id' => 'customer_group_id' // links to related table primary key
 *
 *                  // sorting does not matter, so following also correct:
 *                  'customer_group_id' => 'customer_group_id' // links to related table primary key
 *                  'report_id' => 'entity_id', // links to current table primary key
 *              ],
 *          ]
 *      ]
 * }
 *
 *
 * Class ManyToManyRelation
 */
class Relation_ManyToMany extends Relation_Base
{

    /**
     * save relation
     *
     * @param Varien_Object $model
     * @param boolean $force save relation even if model is not new
     * @return $this
     */
    public function save($values)
    {
        $arrayValues = [];
        if (is_array($values)) {
            $arrayValues = $values;
        } elseif ($values instanceof Varien_Data_Collection) {
            foreach ($values as $item) {
                $arrayValues[] = $item->getData($this->getPk());
            }
        }

        if (is_null($arrayValues)) {
            return $this;
        }

        $oldData = $this->lookup($this->getModelInstance());

        $insert = (array)array_diff($arrayValues, $oldData);
        $delete = (array)array_diff($oldData, $arrayValues);

        $this->update($this->getModelInstance(), $insert, $delete);

        return $this;
    }

    /**
     * lookup relations
     *
     * @param Varien_Object $model
     * @return array
     */
    public function lookup($model)
    {
        $flippedMediatorConfig = array_flip($this->getMediatorConfig());

        $fk = $flippedMediatorConfig[$this->getFkAlias()];
        $pk = $flippedMediatorConfig[$this->getPkAlias()];

        $select = $this->getReadConnection()->select()
            ->from($this->getTable($this->getMediatorTable()), $pk . ' as ' . $this->getPkAlias())
            ->where($fk . ' = :' . $fk);

        $binds = array(
            ':' . $fk => (int)$model->getData($this->getFk())
        );

        return $this->getReadConnection()->fetchCol($select, $binds);
    }

    /**
     * update relation
     *
     * @param Varien_Object $model
     * @param array         $insertData
     * @param array         $removeData
     * @return bool
     */
    public function update($model, $insertData, $removeData)
    {
        $connection = $this->getWriteConnection();
        $connection->beginTransaction();

        try {
            $flippedMediatorConfig = array_flip($this->getMediatorConfig());

            $fk = $flippedMediatorConfig[$this->getFkAlias()];
            $pk = $flippedMediatorConfig[$this->getPkAlias()];

            $table = $this->getTable($this->getMediatorTable());
            if ($removeData) {
                $where = array(
                    $fk . ' = ?' => (int)$model->getData($this->getFk()),
                    $pk . ' IN (?)' => $removeData
                );

                $connection->delete($table, $where);
            }

            if ($insertData) {
                $data = array();

                foreach ($insertData as $value) {
                    $data[] = array(
                        $fk => $model->getData($this->getFk()),
                        $pk => $value
                    );
                }

                $connection->insertMultiple($table, $data);
            }
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * get relation instance
     *
     * @return mixed
     */
    public function getInstance($data=[])
    {
        $model = $this->getModelInstance();
        $data = $this->lookup($model);

        /** @var Varien_Data_Collection_Db $collection */
        $collection = Mage::getResourceModel($this->getModel() . '_collection');
        $collection->addFieldToFilter($this->getPk(), ['in' => $data]);

        if (
            ($collection instanceof Relation_ResourceCollectionInterface) &&
            ($relationName = $collection->getResource()->getRelationNameByModelClassName(get_class($model)))) {
            $collection->setRelationParentModel($relationName, $model);
        }

        return $collection;
    }

    /**
     * get relation
     *
     * @param Varien_Object $object
     * @param string        $name
     * @param array         $relation
     * @return Mage_Customer_Model_Resource_Customer_Collection
     */
    public function loadRelation($object, $name, $relation)
    {
        $relationType = $relation[0];
        $relationModel = $relation[1];
        list($relationFK, $relationPK) = $relation[2];

        switch ($relationType) {
            case 'many_to_many':
                $data = $this->lookupRelation($object, $relation);

                /** @var Varien_Data_Collection_Db $collection */
                $collection = Mage::getModel($relationModel)->getCollection();

                if ($collection instanceof Relation_ResourceCollectionInterface) {
                    if ($data) {
                        $collection->addFieldToFilter($relationPK, ['in' => $data]);
                    } else {
                        $collection->setEmpty();
                    }
                } else {
                $collection->addFieldToFilter($relationPK, ['in' => $data]);
                }


                $this->setData($name, $collection);
                break;
            case 'has_one':
                break;
            case 'has_many':
                break;
            case 'belongs_to':
                break;
        }

    }
}
