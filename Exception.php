<?php
/**
 * @author Misha Medgitov <mdg12v@gmail.com>
 */

/**
 * Relation lib exception
 *
 * @package Relation
 * @author Misha Medgitov <mdg12v@gmail.com>
 */
class Relation_Exception extends Exception
{

}
