<?php
interface Relation_ResourceCollectionInterface
{
    /**
     * set parent model
     *
     * @param string $relationName
     * @param Relation_ModelTrait $model
     * @return $this
     */
    public function setRelationParentModel($relationName, $model);

    /**
     * is relation joined
     *
     * @param string $relationName
     * @return bool
     */
    public function isJoined($relationName);

    /**
     * join relation to query
     *
     * @param string $relationName
     * @param string $joinType
     * @return $this
     */
    public function joinRelation($relationName, $cols = [], $joinType = Zend_Db_Select::INNER_JOIN);
}
