<?php
/**
 *
 * @package Relation
 * @author Misha Medgitov <medgitov@gmail.com>
 */

trait Relation_ResourceModelTrait
{
    /**
     * relations configuration
     *
     * @return array
     */
    abstract public function relations();

    protected $_relationInstances = [];

    /**
     * get relation instance
     *
     * @param string $name
     * @return null|Relation2_Base
     */
    public function getRelation($name, $object)
    {
        if (null !== ($relation = $this->initRelationByName($name))) {
            $relation->setModelInstance($object);
        }

        return $relation;
    }
    /**
     * init relation
     *
     * @param string $name
     * @param array  $config
     * @return Relation|null
     */
    public function initRelation($name, $config)
    {
        if (isset($this->_relationInstances[$name])) {
            return $this->_relationInstances[$name];
        }

        list($relationType, $relationModel) = each($config);
        $relationClass = 'Relation_' . $relationType;

        if (class_exists($relationClass)) {
            $relation = new $relationClass($name, $config);
            return $this->_relationInstances[$name] = $relation;
        } else {
            return null;
        }
    }

    /**
     * init relation by name
     *
     * @param string $name
     * @return Relation|null
     */
    public function initRelationByName($name)
    {
        $relations = $this->relations();
        if (isset($relations[$name])) {
            return $this->initRelation($name, $relations[$name]);
        }

        return null;
    }

    /**
     * get relation name by model name
     *
     * @param $needleModelName
     * @return null|string
     */
    public function getRelationNameByModelClassName($needleModelClassName)
    {
        foreach ($this->relations() as $name => $config) {
            list($relationType, $relationModel) = each($config);
            if ( Mage::getConfig()->getModelClassName($relationModel) == $needleModelClassName) {
                return $name;
            }
        }

        return null;
    }

    /**
     * load relations
     *
     * @param Varien_Object $object
     * @return $this
     */
    public function loadRelations($object, $names = null)
    {
        if ($names && !is_array($names)) {
            $names = [$names];
        }
        foreach ($this->relations() as $name => $config) {
            if ($names && !in_array($name, $names)) {
                continue;
            }
            if ($relation = $this->initRelation($name, $config)) {
                $relation->setModelInstance($object);
                $object->setData($name, $relation->getInstance());
            }
        }

        return $this;
    }
}
