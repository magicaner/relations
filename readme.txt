Example of usage:


======================= How to configure ========================================

======================= Assign to model:

class Magic_Gift_Model_Gift extends Mage_Core_Model_Abstract
{
    use Relation_ModelTrait;
    /**
     * construct
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('magic_gift/gift');
    }
    
    ...
}

======================= Assign to service model and define relations:


class Magic_Gift_Model_Resource_Gift extends Mage_Core_Model_Resource_Db_Abstract
    implements Relation_ResourceModelInterface
{
    use Relation_ResourceModelTrait;
    /**
     * construct
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('magic_gift/gift', 'gift_id');
    }

    /**
     * relations
     *
     * @return array
     */
    public function relations()
    {
        return [
            'products' => [
                Relation_Base::MM => 'catalog/product',
                'gift_id' => 'entity_id:product_id',
                'magic_gift/product' => [
                   'gift_id' => 'gift_id', // links to current table primary key
                   'product_id' => 'product_id' // links to related table primary key
                ]
            ],
            'stores' => [
                Relation_Base::MM => 'core/store',
                'gift_id' => 'store_id',
                'magic_gift/store' => [
                   'gift_id' => 'gift_id', // links to current table primary key
                   'store_id' => 'store_id' // links to related table primary key
                ]
            ],
            'giftproducts' => [
                Relation_Base::HM => 'magic_gift/product',
                'gift_id' => 'gift_id',
            ],
        ];
    }
}

======================= Assign to collection:

class Magic_Gift_Model_Resource_Gift_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
    implements Relation_ResourceCollectionInterface
{
    use Relation_ResourceCollectionTrait;
    /**
     * construct
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('magic_gift/gift');
    }
    
    ...
}


======================= How to use ========================================

======================= Save:
in _afterSave method

foreach ($products as $product) {
    $objects[] = new Varien_Object([
        'product_id' => $_pid,
        'consistent' => 1,
        'position' => 0
    ]);
}

$model->getRelation('giftproducts')->save($objects);
$model->getRelation('stores')->save([1,2,3]);

======================= Read:
 $this->getRelation('giftproducts')->getInstance() // returns magic_gift/product_collection resource model
 $this->getRelation('stores')->getInstance() // returns core/store_collection resource model
 $this->getRelation('products')->getInstance() // returns catalog/product_collection resource model
